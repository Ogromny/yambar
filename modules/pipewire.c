#include <json-c/json.h>
#include <pipewire/impl-metadata.h>
#include <pipewire/pipewire.h>
#include <spa/debug/pod.h>
#include <spa/pod/iter.h>
#include <spa/utils/json.h>
#include <spa/utils/result.h>
#include <stdio.h>
#include <stdlib.h>

#define LOG_MODULE "pipewire"
#define LOG_ENABLE_DBG 1
#include "../config-verify.h"
#include "../config.h"
#include "../module.h"
#include "../particle.h"
#include "../particles/dynlist.h"
#include "../plugin.h"
#include "../yml.h"

#define ARRAY_LENGTH(x) (sizeof((x)) / sizeof((x)[0]))

struct output_informations {
    bool muted;
    /* Raw linear volume (not ceil'd) */
    float volume;
    uint8_t linear_volume;
    uint8_t cubic_volume;
    char *name;
    char *description;
    char *icon;
};
static struct output_informations const output_informations_null;

struct data;
struct private
{
    struct particle *label;
    struct data *data;

    /* pipewire related */
    struct output_informations sink_informations;
    struct output_informations source_informations;
};

/* struct param_to_find */
enum param_to_find_name {
    PARAM_TO_FIND_NAME_MUTE,
    PARAM_TO_FIND_NAME_VOLUME,
};
struct param_to_find {
    uint32_t key;
    uint32_t type;
    enum param_to_find_name name;
};
static struct param_to_find const params_to_find[] = {
    {.key = SPA_PROP_mute, .type = SPA_TYPE_Bool, .name = PARAM_TO_FIND_NAME_MUTE},
    {.key = SPA_PROP_channelVolumes, .type = SPA_TYPE_Array, .name = PARAM_TO_FIND_NAME_VOLUME},
};

/* struct prop_to_find */
enum prop_to_find_name {
    PROP_TO_FIND_NAME_NAME,
    PROP_TO_FIND_NAME_DESCRIPTION,
    PROP_TO_FIND_NAME_ICON,
};
struct prop_to_find {
    char const *key;
    enum prop_to_find_name name;
};
static struct prop_to_find props_to_find[] = {
    {.key = "node.name", .name = PROP_TO_FIND_NAME_NAME},
    {.key = "node.description", .name = PROP_TO_FIND_NAME_DESCRIPTION},
    {.key = "device.icon-name", .name = PROP_TO_FIND_NAME_ICON},
};

/* struct data */
struct node;
struct data {
    /* yambar module */
    struct module *module;

    char *target_sink;
    char *target_source;

    struct node *binded_sink;
    struct node *binded_source;

    /* proxies */
    void *metadata;
    void *node_sink;
    void *node_source;

    /* main struct */
    struct pw_main_loop *loop;
    struct pw_context *context;
    struct pw_core *core;
    struct pw_registry *registry;

    /* listener */
    struct spa_hook registry_listener;
    struct spa_hook core_listener;
    struct spa_hook metadata_listener;
    struct spa_hook node_sink_listener;
    struct spa_hook node_source_listener;

    /* list */
    struct spa_list node_list;

    int sync;
};

/* This struct is needed because when param event occur, the function
 * `node_events_param` will receive the corresponding event about the node
 * but there's no simple way of knowing from which node the event come from */
struct node_data {
    struct data *data;
    /* otherwise is_source */
    bool is_sink;
};
static struct node_data node_data_sink;
static struct node_data node_data_source;

struct node {
    struct spa_list link;
    uint32_t id;
    char *name;
};

/* struct node */
static void
node_unhook_binded_node(struct data *data, bool is_sink)
{
    struct node **target_node = NULL;
    struct spa_hook *target_listener = NULL;
    void **target_proxy = NULL;

    if (is_sink) {
        target_node = &data->binded_sink;
        target_listener = &data->node_sink_listener;
        target_proxy = &data->node_sink;
    } else {
        target_node = &data->binded_source;
        target_listener = &data->node_source_listener;
        target_proxy = &data->node_source;
    }

    if (*target_node == NULL)
        return;

    spa_hook_remove(target_listener);
    pw_proxy_destroy(*target_proxy);

    *target_node = NULL;
    *target_proxy = NULL;
}

static void
node_free(struct node *node, struct data *data)
{
    if (data->binded_sink == node)
        node_unhook_binded_node(data, true);
    else if (data->binded_source == node)
        node_unhook_binded_node(data, false);

    spa_list_remove(&node->link);
    free(node->name);
    free(node);
}

/* Node events */
static void
node_events_info(void *userdata, struct pw_node_info const *info)
{
    struct node_data *node_data = userdata;
    struct data *data = node_data->data;
    struct private *private = data->module->private;

    if (info->change_mask & PW_NODE_CHANGE_MASK_PARAMS) {
        /* We only need the Props param, so let's try to find it */
        for (size_t i = 0; i < info->n_params; ++i) {
            if (info->params[i].id == SPA_PARAM_Props) {
                void *target_node = (node_data->is_sink ? data->node_sink : data->node_source);
                /* Found it, will emit a param event, the parem will then be handled
                 * in node_events_param */
                pw_node_enum_params(target_node, 0, info->params[i].id, 0, -1, NULL);
                break;
            }
        }
    }

    if (info->change_mask & PW_NODE_CHANGE_MASK_PROPS) {
        for (size_t i = 0, j = ARRAY_LENGTH(props_to_find); i < j; ++i) {
            struct prop_to_find target = props_to_find[i];
            struct spa_dict_item const *item = spa_dict_lookup_item(info->props, target.key);

            if (item == NULL)
                continue;

            /* Prop found ! */

            struct output_informations *output_informations
                = (node_data->is_sink ? &private->sink_informations : &private->source_informations);

            switch (target.name) {
            case PROP_TO_FIND_NAME_NAME:
                free(output_informations->name);
                output_informations->name = strdup(item->value);
                break;
            case PROP_TO_FIND_NAME_DESCRIPTION:
                free(output_informations->description);
                output_informations->description = strdup(item->value);
                break;
            case PROP_TO_FIND_NAME_ICON:
                free(output_informations->icon);
                output_informations->icon = strdup(item->value);
                break;
            };

            data->module->bar->refresh(data->module->bar);
        }
    }
}

static void
node_events_param(void *userdata, __attribute__((unused)) int seq, __attribute__((unused)) uint32_t id,
                  __attribute__((unused)) uint32_t index, __attribute__((unused)) uint32_t next,
                  const struct spa_pod *param)
{
    struct node_data *node_data = userdata;
    struct data *data = node_data->data;
    struct private *private = data->module->private;

    struct output_informations *output_informations
        = (node_data->is_sink ? &private->sink_informations : &private->source_informations);

    for (size_t i = 0, j = ARRAY_LENGTH(params_to_find); i < j; ++i) {
        struct param_to_find target = params_to_find[i];
        struct spa_pod_prop const *prop = spa_pod_find_prop(param, NULL, target.key);

        /* Param not found */
        if (prop == NULL)
            continue;

        /* Assert type hasn't changed since current implementation (0.3.56) */
        spa_assert(prop->value.type == target.type);

        switch (target.name) {
            /* Ugly to declare variable here, but I can do case without block now ! */
            bool value;
            uint32_t n_values;
            float *values;
            float total;
        case PARAM_TO_FIND_NAME_MUTE:
            value = false;
            spa_pod_get_bool(&prop->value, &value);
            output_informations->muted = value;
            break;
        case PARAM_TO_FIND_NAME_VOLUME: {
            n_values = 0;
            values = spa_pod_get_array(&prop->value, &n_values);
            total = 0.0f;

            /* array is empty, this means that values haven't changed */
            if (n_values == 0)
                break;

            for (uint32_t i = 0; i < n_values; ++i)
                total += values[i];

            float base_volume = total / n_values;
            output_informations->volume = base_volume * 100;
            output_informations->linear_volume = ceilf(output_informations->volume);
            output_informations->cubic_volume = ceilf(cbrtf(base_volume) * 100);
            break;
        }
        }

        data->module->bar->refresh(data->module->bar);
    }
}

static struct pw_node_events const node_events = {
    PW_VERSION_NODE_EVENTS,
    .info = node_events_info,
    .param = node_events_param,
};

/* Metadata events */
static int
metadata_property(void *userdata, __attribute__((unused)) uint32_t id, char const *key,
                  __attribute__((unused)) char const *type, char const *value)
{
    struct data *data = userdata;
    bool is_sink = false; // true for source mode
    char **target_name = NULL;

    /* We only want default.audio.sink or default.audio.source */
    if (spa_streq(key, "default.audio.sink")) {
        is_sink = true;
        target_name = &data->target_sink;
    } else if (spa_streq(key, "default.audio.source")) {
        is_sink = false; /* just to be explicit */
        target_name = &data->target_source;
    } else
        return 0;

    /* Value is NULL when the profile is set to `off`. */
    if (value == NULL) {
        node_unhook_binded_node(data, is_sink);
        free(*target_name);
        *target_name = NULL;
        data->module->bar->refresh(data->module->bar);
        return 0;
    }

    struct json_object *json = json_tokener_parse(value);
    struct json_object_iterator json_it = json_object_iter_begin(json);
    struct json_object_iterator json_it_end = json_object_iter_end(json);

    while (!json_object_iter_equal(&json_it, &json_it_end)) {
        char const *key = json_object_iter_peek_name(&json_it);
        if (!spa_streq(key, "name")) {
            json_object_iter_next(&json_it);
            continue;
        }

        /* Found name */
        struct json_object *value = json_object_iter_peek_value(&json_it);
        spa_assert(json_object_is_type(value, json_type_string));

        char const *name = json_object_get_string(value);
        /* `auto_null` is the same as `value == NULL` see lines above. */
        if (spa_streq(name, "auto_null")) {
            node_unhook_binded_node(data, is_sink);
            free(*target_name);
            *target_name = NULL;
            data->module->bar->refresh(data->module->bar);
            break;
        }

        /* target_name is the same */
        if (spa_streq(name, *target_name))
            break;

        /* Unhook the binded_node */
        node_unhook_binded_node(data, is_sink);

        /* Update the target */
        free(*target_name);
        *target_name = strdup(name);

        /* Sync the core, core_events_done will then try to bind the good node */
        data->sync = pw_core_sync(data->core, PW_ID_CORE, data->sync);
        break;
    }

    json_object_put(json);

    return 0;
}

static struct pw_metadata_events const metadata_events = {
    PW_VERSION_METADATA_EVENTS,
    .property = metadata_property,
};

/* Registry events */
static void
registry_event_global(void *userdata, uint32_t id, __attribute__((unused)) uint32_t permissions, char const *type,
                      __attribute__((unused)) uint32_t version, struct spa_dict const *props)
{
    struct data *data = userdata;

    /* New node */
    if (spa_streq(type, PW_TYPE_INTERFACE_Node)) {
        /* Fill a new node */
        struct node *node = calloc(1, sizeof(struct node));
        spa_assert(node != NULL);
        node->id = id;
        /* No need to check for nullness Node has always a name */
        node->name = strdup(spa_dict_lookup(props, PW_KEY_NODE_NAME));
        spa_assert(node->name != NULL);

        /* Store it */
        spa_list_append(&data->node_list, &node->link);
    }
    /* New metadata */
    else if (spa_streq(type, PW_TYPE_INTERFACE_Metadata)) {
        /* A metadata has already been bind */
        if (data->metadata != NULL)
            return;

        /* Target only metadata which has "default" key */
        char const *name = spa_dict_lookup(props, PW_KEY_METADATA_NAME);
        if (name == NULL || !spa_streq(name, "default"))
            return;

        /* Bind metadata */
        data->metadata = pw_registry_bind(data->registry, id, type, PW_VERSION_METADATA, 0);
        pw_metadata_add_listener(data->metadata, &data->metadata_listener, &metadata_events, data);
    }

    /* `core_events_done` will then try to bind the good node */
    data->sync = pw_core_sync(data->core, PW_ID_CORE, data->sync);
}

static void
registry_event_global_remove(void *userdata, uint32_t id)
{
    struct data *data = userdata;
    /* Iterator node */
    struct node *node = NULL;
    /* Target node */
    struct node *target_node = NULL;
    spa_list_for_each(node, &data->node_list, link)
    {
        if (node->id == id) {
            /* Must use a target node, because node_free will `spa_list_remove` it
             * from the current looping spa_list, and that's not a good thing to do */
            target_node = node;
            break;
        }
    }

    if (target_node != NULL)
        node_free(target_node, data);
}

static struct pw_registry_events const registry_events = {
    PW_VERSION_REGISTRY_EVENTS,
    .global = registry_event_global,
    .global_remove = registry_event_global_remove,
};

static void
try_to_bind_node(struct node_data *node_data, char const *target_name, struct node **target_node, void **target_proxy,
                 struct spa_hook *target_listener)
{
    /* profile deactived */
    if (target_name == NULL)
        return;

    struct data *data = node_data->data;

    struct node *node = NULL;
    spa_list_for_each(node, &data->node_list, link)
    {
        if (!spa_streq(target_name, node->name))
            continue;

        /* Found good node */

        *target_node = node;
        *target_proxy = pw_registry_bind(data->registry, node->id, PW_TYPE_INTERFACE_Node, PW_VERSION_NODE, 0);
        pw_node_add_listener(*target_proxy, target_listener, &node_events, node_data);
        break;
    }
}

/* Core events */
static void
core_events_done(void *userdata, uint32_t id, int seq)
{
    struct data *data = userdata;

    if (id != PW_ID_CORE)
        return;

    /* Not our seq */
    if (data->sync != seq)
        return;

    /* Sync ended, try to bind the node which has the targeted sink or the targeted source */

    /* Node sink not binded and target_sink is set */
    if (data->binded_sink == NULL && data->target_sink != NULL)
        try_to_bind_node(&node_data_sink, data->target_sink, &data->binded_sink, &data->node_sink,
                         &data->node_sink_listener);

    /* Node source not binded and target_source is set */
    if (data->binded_source == NULL && data->target_source != NULL)
        try_to_bind_node(&node_data_source, data->target_source, &data->binded_source, &data->node_source,
                         &data->node_source_listener);
}

static void
core_events_error(void *userdata, uint32_t id, int seq, int res, char const *message)
{
    pw_log_error("error id:%u seq:%d res:%d (%s): %s", id, seq, res, spa_strerror(res), message);

    if (id == PW_ID_CORE && res == -EPIPE) {
        struct data *data = userdata;
        pw_main_loop_quit(data->loop);
    }
}

static struct pw_core_events const core_events = {
    PW_VERSION_CORE_EVENTS,
    .done = core_events_done,
    .error = core_events_error,
};

/* Signal handler */
static void
do_quit(void *userdata, int signal_number)
{
    struct data *data = userdata;
    pw_main_loop_quit(data->loop);
    kill(getpid(), signal_number);
}

/* init, deinit */
static struct data *
pipewire_init(struct module *module)
{
    pw_init(NULL, NULL);

    /* Data */
    struct data *data = calloc(1, sizeof(struct data));
    spa_assert(data != NULL);

    spa_list_init(&data->node_list);

    /* Main loop */
    data->loop = pw_main_loop_new(NULL);
    spa_assert(data->loop != NULL);
    pw_loop_add_signal(pw_main_loop_get_loop(data->loop), SIGINT, do_quit, data);
    pw_loop_add_signal(pw_main_loop_get_loop(data->loop), SIGTERM, do_quit, data);

    /* Context */
    data->context = pw_context_new(pw_main_loop_get_loop(data->loop), NULL, 0);
    spa_assert(data->context != NULL);

    /* Core */
    data->core = pw_context_connect(data->context, NULL, 0);
    spa_assert(data->core);
    pw_core_add_listener(data->core, &data->core_listener, &core_events, data);

    /* Registry */
    data->registry = pw_core_get_registry(data->core, PW_VERSION_REGISTRY, 0);
    spa_assert(data->registry);
    pw_registry_add_listener(data->registry, &data->registry_listener, &registry_events, data);

    /* Sync */
    data->sync = pw_core_sync(data->core, PW_ID_CORE, data->sync);

    data->module = module;

    /* node_events_param_data */
    node_data_sink.data = data;
    node_data_sink.is_sink = true;
    node_data_source.data = data;
    node_data_source.is_sink = false;

    return data;
}

static void
pipewire_deinit(struct data *data)
{
    struct node *node = NULL;
    spa_list_consume(node, &data->node_list, link) node_free(node, data);

    if (data->metadata)
        pw_proxy_destroy((struct pw_proxy *)data->metadata);
    spa_hook_remove(&data->registry_listener);
    pw_proxy_destroy((struct pw_proxy *)data->registry);
    spa_hook_remove(&data->core_listener);
    spa_hook_remove(&data->metadata_listener);
    pw_core_disconnect(data->core);
    pw_context_destroy(data->context);
    pw_main_loop_destroy(data->loop);
    free(data->target_sink);
    free(data->target_source);
    pw_deinit();
}

static void
destroy(struct module *module)
{
    struct private *private = module->private;

    pipewire_deinit(private->data);
    private->label->destroy(private->label);

    /* sink */
    free(private->sink_informations.name);
    free(private->sink_informations.description);
    free(private->sink_informations.icon);
    /* source */
    free(private->source_informations.name);
    free(private->source_informations.description);
    free(private->source_informations.icon);
    free(private);

    module_default_destroy(module);
}

static char const *
description(struct module *module)
{
    return "pipewire";
}

static struct exposable *
content(struct module *module)
{
    struct private *private = module->private;

    mtx_lock(&module->lock);

    static struct exposable *exposables[2];
    static size_t exposables_length = ARRAY_LENGTH(exposables);

    struct output_informations const *output_informations = NULL;

    /* sink */
    output_informations
        = (private->data->target_sink == NULL ? &output_informations_null : &private->sink_informations);

    struct tag_set sink_tag_set = (struct tag_set){
        .tags = (struct tag *[]){
            tag_new_string(module, "type", "sink"),
            tag_new_string(module, "name", output_informations->name),
            tag_new_string(module, "description", output_informations->description),
            tag_new_string(module, "icon", output_informations->icon),
            tag_new_bool(module, "muted", output_informations->muted),
            tag_new_int_range(module, "linear_volume", output_informations->linear_volume, 0, 100),
            tag_new_int_range(module, "cubic_volume", output_informations->cubic_volume, 0, 100),
        },
        .count = 7,
    };

    /* source */
    output_informations
        = (private->data->target_source == NULL ? &output_informations_null : &private->source_informations);

    struct tag_set source_tag_set = (struct tag_set){
        .tags = (struct tag *[]){
            tag_new_string(module, "type", "source"),
            tag_new_string(module, "name", output_informations->name),
            tag_new_string(module, "description", output_informations->description),
            tag_new_string(module, "icon", output_informations->icon),
            tag_new_bool(module, "muted", output_informations->muted),
            tag_new_int_range(module, "linear_volume", output_informations->linear_volume, 0, 100),
            tag_new_int_range(module, "cubic_volume", output_informations->cubic_volume, 0, 100),
        },
        .count = 7,
    };

    exposables[0] = private->label->instantiate(private->label, &sink_tag_set);
    exposables[1] = private->label->instantiate(private->label, &source_tag_set);

    tag_set_destroy(&sink_tag_set);
    tag_set_destroy(&source_tag_set);

    mtx_unlock(&module->lock);

    return dynlist_exposable_new(exposables, exposables_length, 0, 0);
}

static int
run(struct module *module)
{
    struct private *private = module->private;
    pw_main_loop_run(private->data->loop);
    return 0;
}

static struct module *
pipewire_new(struct particle *label)
{
    struct private *private = calloc(1, sizeof(struct private));
    private->label = label;

    struct module *module = module_common_new();
    module->private = private;
    module->run = &run;
    module->destroy = &destroy;
    module->content = &content;
    module->description = &description;

    private->data = pipewire_init(module);

    return module;
}

static struct module *
from_conf(struct yml_node const *node, struct conf_inherit inherited)
{
    struct yml_node const *content = yml_get_value(node, "content");
    return pipewire_new(conf_to_particle(content, inherited));
}

static bool
verify_conf(keychain_t *keychain, struct yml_node const *node)
{
    static struct attr_info const attrs[] = {
        MODULE_COMMON_ATTRS,
    };
    return conf_verify_dict(keychain, node, attrs);
}

struct module_iface const module_pipewire_iface = {
    .from_conf = &from_conf,
    .verify_conf = &verify_conf,
};

#if defined(CORE_PLUGINS_AS_SHARED_LIBRARIES)
extern struct module_iface const iface __attribute__((weak, alias("module_pipewire_iface")));
#endif
